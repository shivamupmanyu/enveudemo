import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule  } from '@angular/router';
import { HomeComponentComponent } from './home-component/home-component.component';
import { AboutusComponentComponent } from './aboutus-component/aboutus-component.component';
import { ContactusComponentComponent } from './contactus-component/contactus-component.component';
import { ErrorComponentComponent } from './error-component/error-component.component';
import { CreateleadComponentComponent } from './createlead-component/createlead-component.component';
import { ShowleadComponentComponent } from './showlead-component/showlead-component.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponentComponent } from './login-component/login-component.component';  

const myRoutes =[
  {path:"",component:HomeComponentComponent},
  {path:"home",component:HomeComponentComponent},
  {path:"about",component:AboutusComponentComponent},
  {path:"contact",component:ContactusComponentComponent},
  {path:"creatlead",component:CreateleadComponentComponent},
  {path:"showlead",component:ShowleadComponentComponent},
  {path:"registration",component:RegistrationComponent},
  {path:"login",component:LoginComponentComponent},
  {path:"**",component:ErrorComponentComponent}
  ]


@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    AboutusComponentComponent,
    ContactusComponentComponent,
    ErrorComponentComponent,
    CreateleadComponentComponent,
    ShowleadComponentComponent,
    RegistrationComponent,
    LoginComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,RouterModule.forRoot(myRoutes),FormsModule,ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
