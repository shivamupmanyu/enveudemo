import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateleadComponentComponent } from './createlead-component.component';

describe('CreateleadComponentComponent', () => {
  let component: CreateleadComponentComponent;
  let fixture: ComponentFixture<CreateleadComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateleadComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateleadComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
