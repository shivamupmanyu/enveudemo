import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowleadComponentComponent } from './showlead-component.component';

describe('ShowleadComponentComponent', () => {
  let component: ShowleadComponentComponent;
  let fixture: ComponentFixture<ShowleadComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowleadComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowleadComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
