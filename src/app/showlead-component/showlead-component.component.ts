import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-showlead-component',
  templateUrl: './showlead-component.component.html',
  styleUrls: ['./showlead-component.component.css']
})
export class ShowleadComponentComponent implements OnInit {

     resData:any; 
     newsData:any;


  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    
    

    const url = "http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=da4bfccd9a574c3ba48ab4424b55bafc";
    this.http.get(url).subscribe(res=>{
      this.resData = res;
      this.newsData = this.resData.articles;
      console.log(this.newsData);
    })
  }

}
   